const express = require('express'),
    app = express();
require('dotenv').config()
const cors = require('cors');
const router = require('./router')
const port = process.env.PORT;

app.set('view engine','ejs');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use(router);
app.use(express.static(__dirname + "/public"));

app.get('/',(req,res) =>{
    res.render('chapter3');
})
app.get('/game',(req,res) =>{
    res.render('chapter-4');
})

app.listen(port, () =>{
    console.log(`Example app is running on port ${port}`);
})
