const { user_games, user_game_biodata } = require('../models');
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken')

module.exports = {
    list: async (req, res) =>{
        try {
            const data = await user_games.findAll({
                // include :[
                //     {model:user_game_biodata, as: 'user_game_biodata'}
                // ]
        })
            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error);
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const errors = validationResult(req);
                if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
                }
            const hashedPassword = bcrypt.hashSync(req.body.password, 10)
            const data = await user_games.create({
                username: req.body.username,
                password: hashedPassword,
                email: req.body.email,
                user_id: req.body.user_id
            })
            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error);
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
                if (!errors.isEmpty()) {
                console.log(errors);
                return res.status(400).json({ errors: errors.array() });
                }

            const user = await user_games.findOne({where : {
                username: req.body.username
            }})

            if(!user) {
                return res.json({
                    message: 'User does not exist!'
                })
            }

            if(bcrypt.compareSync(req.body.password, user.password)) {
                const token = jwt.sign(
                    { id: user.id },
                    'secret',
                    { expiresIn: '6h' }
                )
                return res.status(200).json({
                    token: token
                })
            }
            
            return res.json({
                message: "Wrong Password!"
            })

        } catch (error) {
            console.log(error);
            return res.json({
                message: 'Fatal Error!'
            })
        }
    },
    getProfile: async (req, res) => {
        try {
            const user = await user_games.findOne({ where : {
                id : res.user.id
            }})
            
            return res.json({
                data : user
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    updatePass: async (req, res) => {
        try {
            const data = await user_games.update({
                password: req.body.password
            },{
                where: {
                    id: req.body.id
                }
            })
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    updateUsername: async (req, res) => {
        try {
            const data = await user_games.update({
                username: req.body.username
            },{
                where: {
                    id: req.body.id
                }
            })
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_games.destroy({
                where: {
                    id: req.body.id
                }
            })
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
}